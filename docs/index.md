# polynom

## (A Short) Description

A package to provide polynomials/polynomial-basis-functions for interpolation

provides routines for

- simple arithmetic
- (vectorized) evaluation
- (vectorized) (higher order) differentiation
- base conversion

supports the following polynomial bases

- [Monomial base](pages/polynom/monomial.md)
- [Newton base](pages/polynom/newton.md)
- [Lagrange base](pages/polynom/lagrange.md)
- ... others (experimentally)

## Getting Started

### Installation

... via pip from PYPI (python package index)
```bash
pip install polynom
```

... or locally (using pip)
```bash
git clone https://gitlab.com/berlinade/polynom.git
cd polynom
pip install -e .
```

### Usage

... *under construction*

## Documentation

... can be found here (*under construction*): [https://berlinade.gitlab.io/polynom/](https://berlinade.gitlab.io/polynom/)

## Find polynom 

- @ GitLab: [https://gitlab.com/berlinade/polynom](https://gitlab.com/berlinade/polynom)
- @ PYPI: [https://pypi.org/project/polynom/](https://pypi.org/project/polynom/)
- auther: [codima on youtube.com](https://www.youtube.com/channel/UCwnthITQqkWgaHnz82U7WsA)

## (Informal Info on) License

this version of the "polynom" has been published under GPLv3 (see [LICENSE](https://gitlab.com/berlinade/polynom/-/blob/main/LICENSE)). <br>
Alternative Licenses are negotiable.
